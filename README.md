# Unit Tests 

## Best practices:
**Readable**: One can understand test under 30s

**Fast**: Do not take long time.

**Isolated**: Fails only when there is an issue with code

**Run often**: No point of having unit tests which are not run frequently.


This code is inspired from a tutorial titeled:

*Learn Java Unit Testing with JUnit 5 in 20 Steps*