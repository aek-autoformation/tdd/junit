package com.mycompany.app;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTimeout;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.Duration;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

/**
 * Unit test for simple App.
 */
public class AppTest 
{   
    /**Runns only once */
    @BeforeAll
    static void beforeAll() {
        System.out.println("Initialize connection to database");
    }

    @AfterAll
    static void afterAll() {
        System.out.println("Close connection to database");
    }
    
    @BeforeEach
    void beforEach(TestInfo info){
        System.out.println("Initialize Test Data Before each test "+ info.getDisplayName());
    }

    @AfterEach
    void afterEach(){
        System.out.println("Clean up Test Data");
    }
    /**
     * Absence of failure is success
     */
    @Test
    @DisplayName("When a string has length 4, the length method should return 4")
    @RepeatedTest(10)
    public void length_basic()
    {
        int actualLength = "ABCD".length();

        int expectedLength = 4;

        assertEquals(expectedLength, actualLength);
    }

    @Test()
    @DisplayName("When length is null, throw an exception")
    public void length_Exception()
    {
        String str = null;

        assertThrows(NullPointerException.class,
        /**Code expected to throw exception */ 
                () -> {
                    str.length();
                }
        /**--------------------------------- */
                    );
    }


    @Test
    @DisplayName("toUpperCase should uppercase all caracters of a string")
    void testUpperCase(){
        String str = "abcd";
        String result = str.toUpperCase();
        assertNotNull(result);
        assertEquals("ABCD", result);
        //assertNotNull(result);
    }

    @Test
    @DisplayName("contains method should return false when a substring does not exist in a string")
    void contains_basic(){
        assertFalse("abcdefgh".contains("ijk"));
    }


    @Test
    @DisplayName("split method should return an array of strings which are seperated by space in input string")
    void split_basic(){
        String str = "abc def ghi";
        String actualResult[] = str.split(" ");
        String[] expectedResults = new String[] { "abc", "def", "ghi"};
        assertArrayEquals(expectedResults,actualResult);
    }

    @Test
    @DisplayName("Verify string length is always greater than 0")
    public void length_greater_than_zero()
    {
        assertTrue("ABCD".length()>0);
        assertTrue("ABC".length()>0);
        assertTrue("A".length()>0);
        assertTrue("DEF".length()>0);
    }

    @ParameterizedTest
    @DisplayName("Verify string length is always greater than 0")
    @ValueSource(strings = {"ABCD", "ABC", "A", "DEF"})
    public void length_greater_than_zero_using_parameterized_test(String str)
    {
        assertTrue(str.length()>0); 
    }

    @ParameterizedTest(name = "{0} to uppercase is {1}")
    @DisplayName("toUpperCase should uppercase all caracters of a string")
    @CsvSource(value = {"abcd, ABCD", "efg, EFG", " '', '' "})
    void testUpperCase_parameterized(String word, String capitalizedWord){
        assertEquals(capitalizedWord, word.toUpperCase());
    }

    @ParameterizedTest(name = "{0} length is {1}")
    @CsvSource(value = {"abcd, 4", "ed, 2"})
    @DisplayName("test the string length ")
    void test_string_length_parameterized(String st, int expectedLength){
        assertEquals(expectedLength, st.length());
    }

    @Test
    @Disabled //Ignored
    void performanceTest(){
        assertTimeout(Duration.ofSeconds(5), 
            
            () -> {
                for (int i = 0; i < 1000; i++) {
                    int j = i;
                    System.out.println(j);
                }
            }
        
                    );
    }

}
